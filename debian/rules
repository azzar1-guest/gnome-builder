#!/usr/bin/make -f

export DEB_BUILD_MAINT_OPTIONS = hardening=+all
export DEB_LDFLAGS_MAINT_APPEND = -Wl,--as-needed

# Explicitly disable this plugin which defaults to enabled
# unless we're on the limited set of architectures that has sysprof.
DEB_HOST_ARCH ?= $(shell dpkg-architecture -qDEB_HOST_ARCH)
ifneq (,$(filter amd64 arm64 armel armhf i386 mips64el mipsel ppc64el alpha powerpcspe x32,$(DEB_HOST_ARCH)))
SYSPROF_PLUGIN = -Dwith_sysprof=true
else
SYSPROF_PLUGIN = -Dwith_sysprof=false
endif

ifneq (,$(filter amd64 arm64 armhf i386 mips mips64 mips64el mipsel powerpc ppc64 ppc64el s390x x32,$(DEB_HOST_ARCH)))
VALGRIND_PLUGIN = -Dwith_valgrind=true
else
VALGRIND_PLUGIN = -Dwith_valgrind=false
endif

ifneq (,$(findstring linux,$(DEB_HOST_ARCH_OS)))
FLATPAK_PLUGIN = -Dwith_flatpak=true
else
FLATPAK_PLUGIN = -Dwith_flatpak=false
endif

%:
	dh $@  --with gir,gnome,python3

override_dh_auto_configure:
	dh_auto_configure -- \
		-Dwith_autotools=true \
		-Dwith_autotools_templates=true \
		-Dwith_beautifier=true \
		-Dwith_cargo=true \
		-Dwith_clang=true \
		-Dwith_cmake=true \
		-Dwith_color_picker=true \
		-Dwith_command_bar=true \
		-Dwith_comment_code=true \
		-Dwith_create_project=true \
		-Dwith_c_pack=true \
		-Dwith_ctags=true \
		-Dwith_eslint=true \
		-Dwith_file_search=true \
		-Dwith_fpaste=true \
		-Dwith_gcc=true \
		-Dwith_gettext=true \
		-Dwith_git=true \
		-Dwith_gnome-code-assistance=true \
		-Dwith_html-completion=true \
		-Dwith_html-preview=true \
		-Dwith_jedi=true \
		-Dwith_jhbuild=true \
		-Dwith_make=true \
		-Dwith_meson=true \
		-Dwith_mono=true \
		-Dwith_meson_templates=true \
		-Dwith_mingw=false \
		-Dwith_newcomers=true \
		-Dwith_phpize=true \
		-Dwith_project-tree=true \
		-Dwith_python-gi-imports-completion=true \
		-Dwith_python-pack=true \
		-Dwith_quick-highlight=true \
		-Dwith_rust-langserv=true \
		-Dwith_support=true \
		-Dwith_symbol-tree=true \
		-Dwith_sysmon=true \
		-Dwith_todo=true \
		-Dwith_vala-pack=true \
		-Dwith_xml-pack=true \
		$(FLATPAK_PLUGIN) \
		$(SYSPROF_PLUGIN) \
		$(VALGRIND_PLUGIN)

# force debian/tmp installation for now until we ship
# more than one binary package, then this can be dropped...
override_dh_auto_install:
	dh_auto_install --destdir=$(CURDIR)/debian/tmp

override_dh_auto_test:
	xvfb-run dh_auto_test || true

override_dh_install:
	rm -rf debian/tmp/usr/lib/python*/*-packages/gi/overrides/__pycache__/
	find debian/tmp -name '*.la' -print -delete
	find debian/tmp/usr/lib/ \( -name '*.pyc' -o -name '*.pyo' \) -print -delete
	rm -rf debian/tmp/usr/include/gnome-builder-*
	dh_install -Xgtk-doc -Xpkgconfig -Xgir-1.0 -Xusr/share/doc/gnome-builder/html

override_dh_missing:
	# see debian/not-installed and bgo#758266 + #804769
	dh_missing --fail-missing -Xpkgconfig

override_dh_makeshlibs:
	dh_makeshlibs -X/usr/lib/$(DEB_HOST_MULTIARCH)/gnome-builder

override_dh_shlibdeps:
	LD_LIBRARY_PATH="debian/gnome-builder/usr/lib/$(DEB_HOST_MULTIARCH)/gnome-builder/" dh_shlibdeps

override_dh_girepository:
	dh_girepository -ldebian/tmp/usr/share/gnome-builder/gir-1.0 \
	  /usr/lib/$(DEB_HOST_MULTIARCH)/gnome-builder/ \
	  /usr/lib/$(DEB_HOST_MULTIARCH)/gnome-builder/girepository-1.0/

override_dh_python3:
# https://bugs.debian.org/838342
	dh_python3 -X.*/plugins/autotools_templates/resources/.* -X.*/plugins/meson_templates/resources/.*
