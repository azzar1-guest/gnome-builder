gnome-builder (3.28.4-1) UNRELEASED; urgency=medium

  * New upstream release
  * Standards-Version: 4.2.0 (no changes required)

 -- Andrea Azzarone <andrea.azzarone@canonical.com>  Tue, 21 Aug 2018 18:55:53 +0300

gnome-builder (3.28.3-1) unstable; urgency=medium

  * Team upload
  * New upstream release
  * d/p/Python-Avoid-identifier-async-which-became-a-keyword-in-3.patch:
    Add patch to avoid async as a Python identifier, for Python 3.7
    compatibility (Closes: #903558)
  * Standards-Version: 4.1.5 (no changes required)
  * Set Rules-Requires-Root to no

 -- Simon McVittie <smcv@debian.org>  Wed, 11 Jul 2018 14:19:07 +0100

gnome-builder (3.28.1-1) unstable; urgency=medium

  * New upstream release
  * Bump Standards-Version to 4.1.4

 -- Jeremy Bicha <jbicha@debian.org>  Tue, 10 Apr 2018 17:28:22 -0400

gnome-builder (3.28.0-1) unstable; urgency=medium

  * New upstream release
  * Release to unstable
  * Bump minimum jsonrpc-glib, libdazzle, and template-glib to 3.28.0
  * Bump minimum glib2.0 to 2.56.0
  * Drop temporary Build-Depends on libclang-5.0-dev
  * Add revert-spellcheck-depend-on-enchant-2.patch:
    - Debian doesn't have the latest gpsell or enchant2 so revert
      this unnecessary version bump

 -- Jeremy Bicha <jbicha@debian.org>  Sat, 17 Mar 2018 09:47:26 -0400

gnome-builder (3.27.90-1) experimental; urgency=medium

  * New upstream development release
    - /usr/bin/gnome-builder-cli is no longer included
  * debian/copyright: Drop obsolete Files-Excluded references
    (embedded code copies are no longer included in gnome-builder releases)
  * debian/control.in:
    - Bump minimum jsonrpc-glib, libdazzle, template-glib, and vala versions
    - Build-Depend on llvm-dev
    - Add a temporary Build-Depends on libclang-5.0-dev to match
      the llvm 5.0 that is being pulled in
  * Update debian/rules for latest build options

 -- Jeremy Bicha <jbicha@debian.org>  Tue, 13 Feb 2018 10:39:46 -0500

gnome-builder (3.26.4-2) unstable; urgency=medium

  * Build with vala 0.38

 -- Jeremy Bicha <jbicha@debian.org>  Tue, 06 Feb 2018 10:35:02 -0500

gnome-builder (3.26.4-1) unstable; urgency=medium

  * New upstream release
  * Bump minimum libdazzle to 3.26.3

 -- Jeremy Bicha <jbicha@debian.org>  Thu, 01 Feb 2018 07:49:41 -0500

gnome-builder (3.26.3-1) unstable; urgency=medium

  * New upstream release
  * Bump minimum libdazzle to 3.26.2
  * Update Vcs fields for migration to https://salsa.debian.org/
  * Build-Depend on debhelper >= 11.1 to automatically build with the C.UTF-8
    locale
  * Bump debhelper compat to 11

 -- Jeremy Bicha <jbicha@debian.org>  Sat, 20 Jan 2018 07:00:16 -0500

gnome-builder (3.26.2-2) unstable; urgency=medium

  * Update Vcs fields for conversion to git
  * Add debian/gbp.conf
  * Bump Standards-Version to 4.1.2

 -- Jeremy Bicha <jbicha@debian.org>  Thu, 14 Dec 2017 12:52:51 -0500

gnome-builder (3.26.2-1) unstable; urgency=medium

  [ Jeremy Bicha ]
  * New upstream release
  * Drop git_protect-against-spurious-builds.patch: Applied in new release

  [ Emilio Pozuelo Monfort ]
  * Build-depend on python3 and python3-gi rather than the development
    packages, as we don't need development headers.
  * Re-enable clang support on armel, as LLVM is fixed there.

 -- Jeremy Bicha <jbicha@debian.org>  Sun, 05 Nov 2017 14:31:52 -0500

gnome-builder (3.26.1-1) unstable; urgency=medium

  * New upstream release
  * debian/control.in: Bump the build-dependencies
  * Add git_protect-against-spurious-builds.patch:
    - Cherry-pick fix for excessive build notifications
  * Add fix-build-with-multiarch.patch, modify gnome-builder.install:
    - Install Ide.py to the correct non-multiarch directory
  * Bump Standards-Version to 4.1.1

 -- Jeremy Bicha <jbicha@debian.org>  Wed, 11 Oct 2017 19:06:57 -0400

gnome-builder (3.26.0-1) unstable; urgency=medium

  * New upstream release
    - debian/control.in: Bump the (build-)dependencies
    - Drop debian/patches/git_gdb-fix-mips-build.patch, merged upstream
    - Drop d/p/revert-require-libgit2-glib25.patch, not needed anymore
  * debian/control.in: gir1.2-git2-glib-1.0 package has been renamed, switch
    to gir1.2-ggit-1.0 instead
  * debian/rules: Use xvfb when running the tests
  * debian/rules: Enable the meson templates
  * debian/control.in: Add Recommends against meson, build-essential and other
    autotools packages so generated project can be build (Closes: #851353)
  * Add debian/gnome-builder.lintian-overrides

 -- Laurent Bigonville <bigon@debian.org>  Sun, 24 Sep 2017 12:50:58 +0200

gnome-builder (3.25.92-2) unstable; urgency=medium

  * Add git_gdb-fix-mips-build.patch:
    - Fix build failure on mips* architectures

 -- Jeremy Bicha <jbicha@debian.org>  Thu, 07 Sep 2017 15:41:37 -0400

gnome-builder (3.25.92-1) unstable; urgency=medium

  * New upstream release
  * Release to unstable
  * debian/control.in:
    - Bump minimum jsonrpc-glib, libdazzle, and template-glib to 3.25.92
    - Drop obsolete Build-Depends on libgjs-dev
  * debian/control.in, Add revert-require-libgit2-glib25.patch:
    - Build with libgit-glib 0.24 since 0.26 is still in experimental
  * debian/copyright:
    - Use Files-Excluded to not include embedded code copies of
      jsonrpc-glib, libdazzle and template-glib in source tarball
    - Some updates

 -- Jeremy Bicha <jbicha@debian.org>  Wed, 06 Sep 2017 08:59:58 -0400

gnome-builder (3.25.91-1) experimental; urgency=medium

  * Only build-dep and enable flatpak plugin on linux
  * Use dh_missing --fail-missing instead of obsolete dh_install....
  * Bump Standards-Version to 4.1.0
  * New upstream beta release.
  * Update build-dependencies according to upstream changes.
  * Drop debian/patches/revert-require-libgit2-glib25.patches
  * Use C.UTF-8 locale when building as C is not supported.
  * Switch configure flags to meson option defines.
  * Install python gi overrides file from dist-packages path.
  * Stop installing dropped ide-* binaries (internals moved to libdazzle).

 -- Andreas Henriksson <andreas@fatal.se>  Wed, 30 Aug 2017 15:52:07 +0200

gnome-builder (3.24.2-1) unstable; urgency=medium

  * New upstream release.
  * debian/control.in:
    - Switch to vala 0.36
    - Add Build-Depends on libgspell-1-dev
    - Bump Build-Depends on sysprof to (>= 3.23.91) as required by the
      sysprof plugin.
    - Recommend flatpak-builder
  * Update configure switches:
    - Add beautifier, cmake, eslint, make, mono, phpize, rust
    - Explicitly disable meson-templates, see GNOME #778897
    - Drop contributing (dropped in new version)
    - Drop build-tools and devhelp (always enabled)
  * Enable valgrind plugin on architectures that have valgrind
    and recommend valgrind on those architectures
  * Don't install local copy of https://builder.readthedocs.io/
    because the local docs will need more work for proper packaging
  * Add revert-require-libgit2-glib25.patch:
    - Don't require version of libgit2-glib not in Debian yet

 -- Jeremy Bicha <jbicha@ubuntu.com>  Wed, 02 Aug 2017 04:11:14 -0400

gnome-builder (3.22.4-1) unstable; urgency=medium

  * New upstream release.
  * Bump Build-Depends on libvte-2.91-dev to (>= 0.46) as required by the
    terminal plugin.
  * Add Build-Depends on libsoup2.4-dev as required by the flatpak plugin.
  * Bump Build-Depends on libflatpak-dev to (>= 0.6.9) as required by the
    flatpak plugin.
  * Update configure switches
    - Rename xdg-app-plugin → flatpak-plugin
    - Rename library-template-plugin → autotools-templates
    - Explicitly enable cargo, color-picker, meson and quick-highlight plugin

 -- Michael Biebl <biebl@debian.org>  Thu, 22 Dec 2016 12:59:44 +0100

gnome-builder (3.22.3-1) unstable; urgency=medium

  * New upstream release.

 -- Andreas Henriksson <andreas@fatal.se>  Wed, 30 Nov 2016 13:37:09 +0100

gnome-builder (3.22.2-1) unstable; urgency=medium

  [ Andreas Henriksson ]
  * Recommend flatpak

  [ Michael Biebl ]
  * New upstream release.
  * Update Build-Depends as per configure.ac.

 -- Michael Biebl <biebl@debian.org>  Sun, 06 Nov 2016 02:28:10 +0100

gnome-builder (3.22.1-1) unstable; urgency=medium

  [ Jeremy Bicha ]
  * Don't use dh_python3 for autotools templates (Closes: #838342)

  [ Emilio Pozuelo Monfort ]
  * Disable clang support on armel, as LLVM 3.8 is not available there.

  [ Michael Biebl ]
  * Upload to unstable.
  * New upstream release.

  [ Andreas Henriksson ]
  * Bump sysprof build-dependency to >= 3.22.1 according to configure.ac
  * Update list of architectures to include all where sysprof as of
    3.22.1-2 has built successfully on.
  * Add dependency on autoconf-archive as users of gnome-builder will
    want to have it installed to be able to build their new C projects.

 -- Andreas Henriksson <andreas@fatal.se>  Sun, 16 Oct 2016 16:49:09 +0200

gnome-builder (3.22.0-1) experimental; urgency=medium

  * debian/rules: fix multiple-condition find delete rule.
  * New upstream release.
  * Bump libgtk-3-dev build-dependency to >= 3.21.6 according to configure.ac
  * Bump dh compat to 10 (automatic dh-autoreconf).

 -- Andreas Henriksson <andreas@fatal.se>  Mon, 19 Sep 2016 21:55:01 +0200

gnome-builder (3.21.91-1) experimental; urgency=medium

  [ Andreas Henriksson ]
  * debian/control*:
    - Limit sysprof build-dependency to where it's currently available.
    - Add sysprof to Recommends:, so it's available if plugin gets enabled.
  * debian/rules:
    - Explicitly pass --{enable,disable}-sysprof-plugin based on arch
      we're building on as the plugin defaults to enable (not auto).

  [ Michael Biebl ]
  * New upstream release.
  * Drop debian/patches/sysprog-plugin-no-so-ver.patch, merged upstream.
  * Update package description.

 -- Michael Biebl <biebl@debian.org>  Tue, 06 Sep 2016 20:16:38 +0200

gnome-builder (3.21.90-1) experimental; urgency=medium

  * New upstream beta release.
  * Update build-dependencies according to configure.ac changes:
    - drop intltool, in favour of gettext.
    - bump gobject-introspection to >= 1.48.0
    - libglib2.0-dev to >= 2.49.0
    - libgtk-3-dev to >= 3.21.1
    - libgtksourceview-3.0-dev to >= 3.21.2
    - python-gi-dev to >= 3.21.0
  * Update build-dependencies as needed by plugins/*/configure.ac:
    - Add libflatpak-dev >= 0.6.0, to have the flatpak plugin enabled.
    - Add sysprof >= 3.21.90, as we need sysprof-ui-2.pc for sysprof plugin.
  * Add mm-common build-dependency as needed by mm-doc.m4
  * Drop debian/patches, now part of upstream release:
    - 0001-app-rename-program-mode-checks-to-gnome-builder-cli.patch
    - 0002-terminal-add-scrollbar-for-terminal.patch
    - 0003-desktop-Add-IDE-category.patch
  * debian/rules: drop renaming of /usr/bin/ide, no longer needed.
  * debian/gnome-builder.install: install gnome-builder-worker from
    libexecdir as it's no longer shipped in /usr/bin upstream.
  * Add debian/patches/sysprog-plugin-no-so-ver.patch
    - this avoids building .so.0* for the sysprof plugin.

 -- Andreas Henriksson <andreas@fatal.se>  Tue, 23 Aug 2016 12:25:39 +0200

gnome-builder (3.20.4-4) unstable; urgency=medium

  * Disable clang plugin on armel, LLVM 3.8 is not available there.

 -- Emilio Pozuelo Monfort <pochu@debian.org>  Fri, 07 Oct 2016 21:26:41 +0200

gnome-builder (3.20.4-3) unstable; urgency=medium

  * Build against vala 0.34.

 -- Michael Biebl <biebl@debian.org>  Wed, 21 Sep 2016 15:46:46 +0200

gnome-builder (3.20.4-2) unstable; urgency=medium

  [ Andreas Henriksson ]
  * Depend on clang to have a solid out-of-the-box experience with C/C++
    - as recommended by upstream
  * Depend on gir1.2-git2-glib-1.0 as needed by contributing_plugin
  * Depend on libvala-0.32-dev as libvala-0.32.pc is needed to find
    libvala-0.32. Fixes warning on console while opening a project.

  [ Jeremy Bicha ]
  * debian/rules, debian/gnome-builder.install, and
    0001-app-rename-program-mode-checks-to-gnome-builder-cli.patch:
    - Rename `/usr/bin/ide` to `gnome-builder-cli` (Closes: #824517)
  * Backport some other fixes from git:
    - 0002-terminal-add-scrollbar-for-terminal.patch
    - 0003-desktop-Add-IDE-category.patch

  [ Laurent Bigonville ]
  * debian/control.in: Also Recommend python3-lxml for jedi plugin (Closes:
    #826559)

 -- Laurent Bigonville <bigon@debian.org>  Fri, 10 Jun 2016 17:41:45 +0200

gnome-builder (3.20.4-1) unstable; urgency=medium

  * New upstream release.

 -- Michael Biebl <biebl@debian.org>  Wed, 11 May 2016 20:26:21 +0200

gnome-builder (3.20.2-1) unstable; urgency=medium

  [ Andreas Henriksson ]
  * New upstream release.
  * Drop debian/patches/0001-vala-remove-in-tree-hack-for-builder-deps.patch
    - merged upstream.
  * Update build-dependencies according to configure.ac changes:
    - Bump gobject-introspection to >= 1.47.92
    - Bump libglib2.0-dev to >= 2.47.92
    - Bump libgtk-3-dev to >= 3.20.0
    - Bump libgtksourceview-3.0-dev to >= 3.20.0
    - Bump libpeas-dev to >= 1.17.0
    - Bump python-gi-dev to >= 3.19.3
    - Bump valac to >= 0.30.0.55
    - Add yelp-tools
  * Change dependency from valac-0.30-vapi to valac-0.32-vapi
    (Closes: #819538)
  * Change libvala-0.30-dev build-dependency to -0.32 to match.
  * Bump Standards-Version to 3.9.8
  * Ship new gnome-builder-worker and ide commands
  * Ship new help files
  * Add debian/patches/fix-terminal-plugin-ldflags.patch
    - avoids generating so-versioned libterminal.so.*
  * debian/rules: delete all *.pyc (and *.pyo) files in usr/lib
    - avoids precompiled site-packages python files in package.
  * debian/rules: delete usr/include/gnome-builder-*
    - files exists to allow gir, vala, etc bindings generation in-tree.
  * Install usr/lib/python3.5/site-packages/gi/overrides for Ide.py override
    - see also http://bugs.debian.org/819736
  * Use dh-autoreconf, needed since we patch plugins/terminal/Makefile.am
    - this replaces autotools-dev which gets dropped
  * Explicitly enable git plugin (which was formerly not a plugin)
  * Bump build-dependencies for plugins/*/configure.ac changes:
    - devhelp plugin: libdevhelp-dev to >= 3.20.0
    - git plugin: libgit2-glib-1.0-dev to >= 0.24.0
    Note: added/new plugins not taken into consideration here (yet).
  * Add debian/patches/fix-missing-git-check.patch
  * Drop explicit disabling of device-manager plugin, removed upstream.
  * Explicitly enable all new plugins:
    - build-tools-plugin, comment-code-plugin, contributing-plugin,
      create-project-plugin, fpaste-plugin, gcc-plugin, gettext-plugin,
      jhbuild-plugin, library-template-plugin, project-tree-plugin,
      support-plugin, todo-plugin, xdg-app-plugin
  * Build-dep on dh-python, use --with python3 and dep on ${python3:Depends}.
    - according to man dh_python3 for fixing up module path.
  * Remove gi override pycache and use py3compile in postinst.
  * Bump libpeas build-dependency as we need 1.18.0 at runtime
    - otherwise the main 'editor' plugin will not be found and all breaks.

  [ Michael Biebl ]
  * Drop uploaders.mk from debian/rules as this breaks the clean target with
    dh. Instead use the gnome dh addon which updates debian/control via
    dh_gnome_clean.

  [ Laurent Bigonville ]
  * New upstream release (3.20.2).
    - Drop all patches, merged upstream
    - Add libpango1.0-dev to the build-dependencies

 -- Andreas Henriksson <andreas@fatal.se>  Wed, 11 May 2016 09:22:28 +0200

gnome-builder (3.18.1-2) unstable; urgency=medium

  [ Tim Lunn ]
  * debian/control.in: Add build-dep on libwebkit2gtk-4.0-dev this
    is required by the HTML preview plugin
  * debian/rules:
    - Explicitly enable all plugins (except experimental plugins)
    - switch to fail-missing, remove plugin .la files and exclude the libide
      bits, this still has unstable ABI and not meant to be public yet
      (Closes: #804769) (bgo: #758266)

 -- Andreas Henriksson <andreas@fatal.se>  Fri, 20 Nov 2015 10:49:15 +0100

gnome-builder (3.18.1-1) unstable; urgency=medium

  * New upstream release
    - Drop all the patches, merged upstream
    - Adjust the build-dependencies
  * Automatically determine the dependencies for the .typelib files (Closes:
    #801648)
  * Exclude private libraries from dh_makeshlibs call
  * List files currently not installed in the package and install some of them
  * debian/rules: Do not build static libraries
  * d/p/0001-vala-remove-in-tree-hack-for-builder-deps.patch: remove in-tree
    hack for builder deps, this removes some runtime error, from upstream

 -- Laurent Bigonville <bigon@debian.org>  Thu, 15 Oct 2015 22:19:42 +0200

gnome-builder (3.16.3-2) unstable; urgency=medium

  * Upload to unstable.
  * Use unversioned libclang-dev Build-Depends to ensure it matches the llvm
    version. This also ensures we get the Debian default llvm/clang version.

 -- Michael Biebl <biebl@debian.org>  Wed, 14 Oct 2015 17:55:23 +0200

gnome-builder (3.16.3-1) experimental; urgency=low

  * Initial release

 -- Andreas Henriksson <andreas@fatal.se>  Sun, 11 Oct 2015 12:38:45 +0200
